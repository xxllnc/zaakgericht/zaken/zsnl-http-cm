# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView

log = logging.getLogger(__name__)


class CustomObjectTypeViews(JSONAPIEntityView):
    # Move this away in new version, and do this sexyer
    def create_link_from_entity(
        self, entity=None, entity_type=None, entity_id=None
    ):
        if entity is not None:
            entity_type = entity.entity_type
            entity_id = str(entity.entity_id)

        if entity_type == "custom_object_type":
            return self.request.route_url(
                "get_custom_object_type",
                _query={"uuid": entity_id},
                _scheme="https",
            )

    # Move this to openapi.json
    view_mapper = {
        "GET": {
            "get_custom_object_type": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_custom_object_type_by_uuid",
                "from": {"request_params": {"uuid": "uuid"}},
            },
            "get_persistent_custom_object_type": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_custom_object_type_by_version_independent_uuid",
                "from": {"request_params": {"uuid": "uuid", "use": "use"}},
            },
        },
        "POST": {
            "create_custom_object_type": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_custom_object_type",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "custom_object_type",
            },
            "update_custom_object_type": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_custom_object_type",
                "from": {
                    "request_params": {"uuid": "existing_uuid"},
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "custom_object_type",
            },
            "delete_custom_object_type": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "delete_custom_object_type",
                "from": {
                    "json": {
                        "uuid": "uuid",
                        "version_independent_uuid": "version_independent_uuid",
                    }
                },
            },
        },
    }
