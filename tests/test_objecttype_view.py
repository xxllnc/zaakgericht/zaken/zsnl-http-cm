# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_case_management_http.views import custom_object_type


class TestObjectTypeViews:
    def test_create_link_from_entity(self):
        request = mock.MagicMock()
        entity = mock.MagicMock()
        ot_uuid = uuid4()
        type(entity).entity_type = mock.PropertyMock(
            return_value="custom_object_type"
        )
        type(entity).entity_id = mock.PropertyMock(return_value=ot_uuid)
        ov = custom_object_type.CustomObjectTypeViews(
            context="test", request=request
        )

        ov.create_link_from_entity(entity)

        request.route_url.assert_called_once_with(
            "get_custom_object_type",
            _query={"uuid": str(ot_uuid)},
            _scheme="https",
        )
